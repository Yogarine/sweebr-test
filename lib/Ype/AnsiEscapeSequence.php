<?php
/*  Ype - A PHP command line environment.
    Copyright © 2011-2015 Alwin Garside
    All rights reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

        1. Redistributions of source code must retain the above copyright
           notice, this list of conditions and the following disclaimer.

        2. Redistributions in binary form must reproduce the above copyright
           notice, this list of conditions and the following disclaimer in the
           documentation and/or other materials provided with the distribution.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
    IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
    THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
    PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
    CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
    EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
    PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
    OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
    WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
    OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
    ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

    The views and conclusions contained in the software and documentation are
    those of the authors and should not be interpreted as representing official
    policies, either expressed or implied, of the copyright holders. */

/**
 * @package   Ype
 * @author	  Alwin Garside <alwin@garsi.de>
 * @copyright Copyright © 2011-2015 Alwin Garside
 * @license   http://www.freebsd.org/copyright/freebsd-license.html FreeBSD License
 */

/**
 * @package Ype
 */
class Ype_AnsiEscapeSequence extends Ype_ASCII
{
	// VT100 Mode
	/** 7-bit controls */
	const S7C1T = ' F';
	/** 8-bit controls */
	const S8C1T = ' G';
	/** DEC double-height line, top half */
	const DECDHL_TOP = '#3';
	/** DEC double-height line, bottom half */
	const DECDHL_BOTTOM = '#4';
	/** Save Cursor */
	const DECSC = '7';
	/** Restore Cursor */
	const DECRC = '8';

	const START_CSI_CODE = 64;
	const END_CSI_CODE   = 126;

	const SS3_CUU  = 'OA';
	const SS3_CUD  = 'OB';
	const SS3_CUF  = 'OC';
	const SS3_CUB  = 'OD';
	const SS3_HOME = 'OH';
	const SS3_END  = 'OF';

	const CSI_HOME      = '[1~';
	const CSI_INSERT    = '[2~';
	const CSI_DELETE    = '[3~';
	const CSI_END       = '[4~';
	const CSI_PAGE_UP   = '[5~';
	const CSI_PAGE_DOWN = '[6~';

	const CSI_F1  = '[11~';
	const CSI_F2  = '[12~';
	const CSI_F3  = '[13~';
	const CSI_F4  = '[14~';
	const CSI_F5  = '[15~';
	const CSI_F6  = '[17~';
	const CSI_F7  = '[18~';
	const CSI_F8  = '[19~';
	const CSI_F9  = '[20~';
	const CSI_F10 = '[21~';
	const CSI_F11 = '[23~';
	const CSI_F12 = '[24~';
	const CSI_F13 = '[25~';
	const CSI_F14 = '[26~';
	const CSI_F15 = '[28~';
	const CSI_F16 = '[29~';
	const CSI_F17 = '[31~';
	const CSI_F18 = '[32~';
	const CSI_F19 = '[33~';
	const CSI_F20 = '[34~';

	/** Insert (Blank) Character(s) */
	const CSI_ICH = '[%d@';
	/** Cursor Up */
	const CSI_CUU = '[%dA';
	/** Cursor Down */
	const CSI_CUD = '[%dB';
	/** Cursor Forward */
	const CSI_CUF = '[%dC';
	/** Cursor Backwards */
	const CSI_CUB = '[%dD';
	/** Cursor Next Line */
	const CSI_CNL = '[%dE';
	/** Go to end of line. */
	const CSI_CPL_END = '[F';
	/** Cursor Previous Line */
	const CSI_CPL = '[%1$dF';
	/** Cursor Character Absolute */
	const CSI_CHA = '[%dG';
	const CSI_CUP_HOME = '[H';
	/** Cursor Position [row;column] */
	const CSI_CUP = '[%1$d;%2$dH';
	/** Cursor Forward Tabulation */
	const CSI_CHT = '[%dI';
	/** Erase Display */
	const CSI_ED = '[%dJ';
	/** Selective Erase in Display */
	const CSI_DECSED = '[?%dJ';
	/** Erase in Line */
	const CSI_EL = '[%dK';
	/** Selective Erase in Line */
	const CSI_DECSEL = '[?%dK';
	/** Insert Line(s) */
	const CSI_IL  = '[%dL';
	/** Delete Line(s) */
	const CSI_DL  = '[%dM';
	/** Delete Character(s) */
	const CSI_DCH = '[%dP';
	/** Cursor Position Report */
	const CSI_CPR = '[%d;%dR';
	/** Scroll Up */
	const CSI_SU  = '[%dS';
	/** Scroll Down */
	const CSI_SD  = '[%dT';
	/** Initiate highlight mouse tracking. */
	const CSI_MOUSE_TRACKING = '[%d;%d;%d;%d;%dT';
	/** Erase Character(s) */
	const CSI_ECH = '[%dX';
	/** Cursor Backward Tabulation */
	const CSI_CBT = '[%dZ';
	/** Horizontal Position Absolute */
	const CSI_HPA = '[%d`';
	/** Repeat.
	 * Repeat the preceding graphic character %d times. */
	const CSI_REP = '[%db';
	/** Device Attributes */
	const CSI_DA1 = '[%dc';
	/** Device Attributes */
	const CSI_DA2 = '[>%dc';
	/** Horizontal and Vertical Position */
	const CSI_HVP = '[%d;%df';
	/** Set Graphics Rendition */
	const CSI_SGR = '[%sm';
	/** Device Status Report - OK */
	const CSI_DSR_OK = '[0n';
	/** Device Status Report - Status Report */
	const CSI_DSR_STATUS_REPORT = '[5n';
	/** Device Status Report - Cursor Position Report*/
	const CSI_DSR_CPR = '[6n';
	/** Save Cursor Position */
	const CSI_SCP = '[s';
	/** Window manipulation - Resize the text area to given height and width in characters.
	 * Omitted parameters reuse the current height or width.
	 * Zero parameters use the display’s height or width. */
	const CSI_WM_RESIZE_TEXT_AREA = '[8;%d;%dt';
	/** Window manipulation - Report the size of the area in characters. The terminal emulator returns [8;<h>;<w>t. */
	const CSI_WM_REPORT_TEXT_AREA_SIZE = '[18t';
	/** Window manipulation - Report the size of the screen in characters. Result is [9;<h>;<w>t. */
	const CSI_WM_REPORT_SCREEN_SIZE = '[19t';
	/** Restore Cursor Position */
	const CSI_RCP = '[u';

	/** Erase Below (default) */
	const ED_BELOW       = 0;
	/** Erase Above */
	const ED_ABOVE       = 1;
	/** Erase All */
	const ED_ALL         = 2;
	/** Erase Saved Lines (xterm) */
	const ED_SAVED_LINES = 3;

	/** Selective Erase Below (default) */
	const DECSED_BELOW = 0;
	/** Selective Erase Above */
	const DECSED_ABOVE = 1;
	/** Selective Erase All */
	const DESCED_ALL   = 2;

	/** Erase to Right (default) */
	const EL_RIGHT = 0;
	/** Erase to Left */
	const EL_LEFT  = 1;
	/** Erase All */
	const EL_ALL   = 2;

	/** Selective Erase to Right (default) */
	const DECSEL_RIGHT = 0;
	/** Selective Erase to Left */
	const DECSEL_LEFT  = 1;
	/** Selective Erase All */
	const DECSEL_ALL   = 2;

	/** Set Graphics Rendition - All attributes off */
	const SGR_ALL_OFF = 0;
	/** Set Graphics Rendition - Bold ON */
	const SGR_BOLD = 1;
	/** Set Graphics Rendition - Faint ON */
	const SGR_FAINT = 2;
	/** Set Graphics Rendition - Italic ON */
	const SGR_ITALIC = 3;
	/** Set Graphics Rendition - Blink ON */
	const SGR_BLINK = 5;
	/** Set Graphics Rendition - Reverse Video ON */
	const SGR_REVERSE_VIDEO = 7;
	/** Set Graphics Rendition - Concealed ON */
	const SGR_CONCEALED = 8;
	/** Set Graphics Rendition - Black foreground */
	const SGR_FOREGROUND_BLACK = 30;
	/** Set Graphics Rendition - Red foreground */
	const SGR_FOREGROUND_RED = 31;
	/** Set Graphics Rendition - Green foreground */
	const SGR_FOREGROUND_GREEN = 32;
	/** Set Graphics Rendition - Yellow foreground */
	const SGR_FOREGROUND_YELLOW = 33;
	/** Set Graphics Rendition - Blue foreground */
	const SGR_FOREGROUND_BLUE = 34;
	/** Set Graphics Rendition - Magenta foreground */
	const SGR_FOREGROUND_MAGENTA = 35;
	/** Set Graphics Rendition - Cyan foreground */
	const SGR_FOREGROUND_CYAN = 36;
	/** Set Graphics Rendition - White foreground */
	const SGR_FOREGROUND_WHITE = 37;
	/** Set Graphics Rendition - Black background */
	const SGR_BACKGROUND_BLACK = 40;
	/** Set Graphics Rendition - Red background */
	const SGR_BACKGROUND_RED = 41;
	/** Set Graphics Rendition - Green background */
	const SGR_BACKGROUND_GREEN = 42;
	/** Set Graphics Rendition - Yellow background */
	const SGR_BACKGROUND_YELLOW = 43;
	/** Set Graphics Rendition - Blue background */
	const SGR_BACKGROUND_BLUE = 44;
	/** Set Graphics Rendition - Magenta background */
	const SGR_BACKGROUND_MAGENTA = 45;
	/** Set Graphics Rendition - Cyan background */
	const SGR_BACKGROUND_CYAN = 46;
	/** Set Graphics Rendition - White background */
	const SGR_BACKGROUND_WHITE = 47;
	/** Set Graphics Rendition - Subscript */
	const SGR_SUBSCRIPT = 48;
	/** Set Graphics Rendition - Superscript */
	const SGR_SUPERSCRIPT = 49;

	const SM_40_X_25_BW      = 0;
	const SM_40_X_25_COLOR   = 1;
	const SM_80_X_25_BW      = 2;
	const SM_80_X_25_COLOR   = 3;
	const SM_320_X_200_COLOR = 4;
	const SM_320_X_200_BW    = 5;
	const SM_640_X_200_BW    = 6;
	const SM_WRAP            = 7;

	private static $_REGULAR_EXPRESSION_PATTERN_REPLACEMENTS = array(
		'/([%d;]+)/' => '($1)?',
		'/%d/'       => '\d*',
		'/%\d$d/'    => '\d+',
	);

	/** @var boolean Whether the STDIN stream buffer is empty. */
	protected static $_stdinBufferIsEmpty = true;

	/** @var string[] List of CSI patterns. */
	private static $_csiPatterns = null;

	/** @var string[] List of CSI regular expression patterns. */
	private static $_csiRegularExpressionPatterns = null;

	/** @var string[] */
	private static $_csiPregPatterns     = null;
	private static $_csiPregReplacements = null;

	/**
	 * @param string $csiPattern
	 * @param array  $args
	 * @param null   $_
	 * @return string
	 */
	public static function getCsiEscapeSequence($csiPattern, $args = null, $_ = null)
	{
		return chr(self::ESC) . sprintf($csiPattern, $args, $_);
	}

	/**
	 * @return string[]
	 */
	public static function getCsiPatterns()
	{
		if(null === self::$_csiPatterns)
		{
			self::$_csiPatterns = array();
			$reflectionClass    = new ReflectionClass(__CLASS__);
			foreach($reflectionClass->getConstants() as $name => $value)
			{
				if(strpos($name, 'CSI_') === 0)
				{
					self::$_csiPatterns[$name] = $value;
				}
			}
		}

		return self::$_csiPatterns;
	}

	/**
	 * @return string[]
	 */
	public static function getCsiRegularExpressionPatterns()
	{
		if(null === self::$_csiRegularExpressionPatterns)
		{
			$search  = array_keys(self::$_REGULAR_EXPRESSION_PATTERN_REPLACEMENTS);
			$search  = array_filter($search, 'preg_quote');
			$replace = array_values(self::$_REGULAR_EXPRESSION_PATTERN_REPLACEMENTS);
			$replace = array_filter($replace, 'preg_quote');

			self::$_csiRegularExpressionPatterns = array();
			foreach(self::getCsiPatterns() as $name => $pattern)
			{
				$pattern = preg_quote($pattern);
				$pattern = preg_replace($search, $replace, $pattern);
				self::$_csiRegularExpressionPatterns[$name] = "/^{$pattern}$/";
			}
		}

		return self::$_csiRegularExpressionPatterns;
	}

	public static function getCsiPregPatternReplacements()
	{
		if(null === self::$_csiPregPatterns || null === self::$_csiPregReplacements)
		{
			$csiPatterns                  = self::getCsiPatterns();
			$csiRegularExpressionPatterns = self::getCsiRegularExpressionPatterns();

			self::$_csiPregPatterns     = array();
			self::$_csiPregReplacements = array();
			foreach($csiRegularExpressionPatterns as $name => $pattern)
			{
				self::$_csiPregPatterns[]     = $pattern;
				self::$_csiPregReplacements[] = $csiPatterns[$name];
			}
		}

		return array(self::$_csiPregPatterns, self::$_csiPregReplacements);
	}

	/**
	 * Returns the pattern that matches an escape sequence that was read from STDIN.
	 *
	 * @param  string  $sequence  The sequence that you want the pattern for
	 * @return string             The pattern for given sequence
	 */
	public static function getPatternForSequence($sequence)
	{
		list($patterns, $replacements) = self::getCsiPregPatternReplacements();
		$pattern = preg_replace($patterns, $replacements, $sequence);
		Ype_Log::debugFunctionCall($pattern);

		return $pattern;
	}

	/**
	 * @param  int   $amount
	 * @return void
	 */
	public static function cursorDown($amount)
	{
		Ype_Log::verbose('ASCII', "cursorDown({$amount})");
		if($amount > 0)
		{
			self::printEscapeSequence(sprintf(self::ESC_CSI_CUD, $amount));
		}
		elseif($amount < 0)
		{
			self::printEscapeSequence(sprintf(self::ESC_CSI_CUU, -$amount));
		}
	}

	/**
	 * @param  int   $amount
	 * @return string
	 */
	public static function cursorForward($amount)
	{
		Ype_Log::verbose('ASCII', "cursorForward({$amount})");
		if($amount > 0)
		{
			return sprintf(self::CSI_CUF, $amount);
		}
		elseif($amount < 0)
		{
			return sprintf(self::CSI_CUB, -$amount);
		}
	}

	/**
	 * @param  int   $amount
	 * @return string
	 */
	public static function cursorBackwards($amount)
	{
		Ype_Log::verbose('ASCII', "cursorBackwards({$amount})");
		if($amount > 0)
		{
			return sprintf(self::CSI_CUB, $amount);
		}
		elseif($amount < 0)
		{
			return sprintf(self::CSI_CUF, -$amount);
		}
	}

	/**
	 * Adds $amount new lines and places the cursor at the beginning of the line.
	 *
	 * @param  int   $amount
	 * @return string
	 */
	public static function cursorNewLine($amount)
	{
		Ype_Log::verbose('ASCII', "cursorBackwards({$amount})");
		if($amount >= 0)
		{
			return sprintf(self::CSI_CNL, $amount);
		}
		elseif($amount < 0)
		{
			return sprintf(self::CSI_CPL, -$amount);
		}
	}
} 