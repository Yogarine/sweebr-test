<?php
/*  Ype - A PHP command line environment.
    Copyright © 2011-2015 Alwin Garside
    All rights reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

        1. Redistributions of source code must retain the above copyright
           notice, this list of conditions and the following disclaimer.

        2. Redistributions in binary form must reproduce the above copyright
           notice, this list of conditions and the following disclaimer in the
           documentation and/or other materials provided with the distribution.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
    IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
    THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
    PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
    CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
    EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
    PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
    OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
    WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
    OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
    ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

    The views and conclusions contained in the software and documentation are
    those of the authors and should not be interpreted as representing official
    policies, either expressed or implied, of the copyright holders. */

/**
 * @package   Ype\Plugin
 * @author	  Alwin Garside <alwin@garsi.de>
 * @copyright Copyright © 2011-2015 Alwin Garside
 * @license   http://www.freebsd.org/copyright/freebsd-license.html FreeBSD License
 */

/**
 * @package Ype\Plugin
 */
class Ype_Plugin_WebSocketServer extends Ype_AbstractPlugin
{
	const PROTOCOL_HTTP      = 'http';

	const PROTOCOL_WEBSOCKET = 'websocket';

	const WEBSOCKET_MAGIC_STRING = '258EAFA5-E914-47DA-95CA-C5AB0DC85B11';

	protected $streamHandler;

	/** @var Ype_NonBlockingStream_Socket_Server */
	protected $server = null;

	protected $messageBuffers = array();

	protected $socketProtocols = array();

	public function __construct(Ype_NonBlockingStream_Handler $streamHandler)
	{
		$this->streamHandler = $streamHandler;
	}

	public function onInitializePlugin()
	{
		if(!$this->isReady)
		{
			$this->server = new Ype_NonBlockingStream_Socket_Server($this->streamHandler, '0.0.0.0', 31337, AF_INET, SOCK_STREAM,
			                                                        SOL_TCP, 'tcp', Ype_NonBlockingStream_Reader::READ_MODE_EOF);
//			$this->server->registerConnectCallback(array($this, 'connect'));
			$this->server->registerReadCallback(array($this, 'receiveData'));
			$this->server->run();

			$this->ready();
		}
	}

	public function onWriteToTextBox($data)
	{
		$this->server->appendOutgoingData("{$data}\n");
	}

	public function onQuit($message = '')
	{
		$this->server->stop();
		parent::onQuit($message);
	}

	protected function upgradeProtocol($protocol, $socketIdentifier)
	{
		Ype_Log::debugFunctionCall();

		switch(strtolower($protocol))
		{
			case 'websocket':
				$this->socketProtocols[$socketIdentifier] = self::PROTOCOL_WEBSOCKET;
		}
	}

	protected function calculateAccept($key)
	{
		$key .= self::WEBSOCKET_MAGIC_STRING;
		$hash = sha1($key);

		$rawToken = "";
		for($i = 0; $i < 20; $i++)
		{
			$rawToken .= chr(hexdec(substr($hash, $i * 2, 2)));
		}

		$token = base64_encode($rawToken);

		Ype_Log::debugFunctionCall($token);

		return $token;
	}

	protected function calculateOffset($headers)
	{
		$offset = 2;
		if($headers['hasmask'])
		{
			$offset += 4;
		}
		if($headers['length'] > 65535)
		{
			$offset += 8;
		}
		elseif($headers['length'] > 125)
		{
			$offset += 2;
		}

		return $offset;
	}

	//check packet if he have more than one frame and process each frame individually
	protected function splitPacket($packet)
	{
		$length        = strlen($packet);
		$fullPacket    = $packet;
		$framePosition = 0;
		$frameId       = 1;

		while($framePosition < $length)
		{
			$headers     = $this->extractHeaders($packet);
			$headersSize = $this->calculateOffset($headers);
			$frameSize   = $headers['length'] + $headersSize;

			// Split frame from packet and process it
			$frame = substr($fullPacket, $framePosition, $frameSize);
			if(($message = $this->deframe($frame, $headers)) !== false)
			{
				if((preg_match('//u', $message)) || ($headers['opcode'] == 2))
				{
					$this->processMessage($message);
				}
			}

			// Get the new position also modify packet data.
			$framePosition += $frameSize;
			$packet = substr($fullPacket, $framePosition);
			$frameId++;
		}
	}

	protected function deframe($message, $headers)
	{
		$pongReply = false;
		$willClose = false;

		switch($headers['opcode'])
		{
			case 0:
			case 1:
			case 2:
				break;
			case 8:
				// TODO close the connection
				return "";
			case 9:
				$pongReply = true;
				break;
			case 10:
				break;
			default:
				// TODO fail connection
				$willClose = true;
				break;
		}
		/* Deal by split_packet() as now deframe() do only one frame at a time.
		if ($user->handlingPartialPacket) {
		  $message = $user->partialBuffer . $message;
		  $user->handlingPartialPacket = false;
		  return $this->deframe($message, $user);
		}
		*/

		if($this->checkRSVBits($headers))
		{
			return false;
		}
		if($willClose)
		{
			// todo: fail the connection
			return false;
		}
		$payload = $user->partialMessage . $this->extractPayload($message, $headers);
		if($pongReply)
		{
			$reply = $this->frame($payload, $user, 'pong');
			socket_write($user->socket, $reply, strlen($reply));

			return false;
		}
		if($headers['length'] > strlen($this->applyMask($headers, $payload)))
		{
			$user->handlingPartialPacket = true;
			$user->partialBuffer         = $message;

			return false;
		}
		$payload = $this->applyMask($headers, $payload);
		if($headers['fin'])
		{
			$user->partialMessage = "";

			return $payload;
		}
		$user->partialMessage = $payload;

		return false;
	}

	protected function checkRSVBits($headers)
	{
		if(ord($headers['rsv1']) + ord($headers['rsv2']) + ord($headers['rsv3']) > 0)
		{
			// TODO fail connection
			return true;
		}

		return false;
	}

	protected function extractHeaders($message)
	{
		$header = array(
			'fin'     => $message[0] & chr(128),
			'rsv1'    => $message[0] & chr(64),
			'rsv2'    => $message[0] & chr(32),
			'rsv3'    => $message[0] & chr(16),
			'opcode'  => ord($message[0]) & 15,
			'hasmask' => $message[1] & chr(128),
			'length'  => 0,
			'mask'    => "",
		);

		$header['length'] = (ord($message[1]) >= 128) ? ord($message[1]) - 128 : ord($message[1]);

		if($header['length'] == 126)
		{
			if($header['hasmask'])
			{
				$header['mask'] = $message[4] . $message[5] . $message[6] . $message[7];
			}
			$header['length'] = ord($message[2]) * 256
			                    + ord($message[3]);
		}
		elseif($header['length'] == 127)
		{
			if($header['hasmask'])
			{
				$header['mask'] = $message[10] . $message[11] . $message[12] . $message[13];
			}
			$header['length'] = ord($message[2]) * 65536 * 65536 * 65536 * 256
			                    + ord($message[3]) * 65536 * 65536 * 65536
			                    + ord($message[4]) * 65536 * 65536 * 256
			                    + ord($message[5]) * 65536 * 65536
			                    + ord($message[6]) * 65536 * 256
			                    + ord($message[7]) * 65536
			                    + ord($message[8]) * 256
			                    + ord($message[9]);
		}
		elseif($header['hasmask'])
		{
			$header['mask'] = $message[2] . $message[3] . $message[4] . $message[5];
		}

		return $header;
	}

	public function receiveData($data, $socketIdentifier)
	{
		Ype_Log::debugFunctionCall();

		if(!isset($this->socketProtocols[$socketIdentifier]))
		{
			$this->socketProtocols[$socketIdentifier] = self::PROTOCOL_HTTP;
		}

		switch($this->socketProtocols[$socketIdentifier])
		{
			case self::PROTOCOL_HTTP:
				$lines = explode("\n", $data);
				$headers = array();
				$query = array_shift($lines);

				foreach($lines as $line)
				{
					if(strpos($line, ':') !== false)
					{
						list($key, $value) = explode(':', $line, 2);
						$headers[strtolower(trim($key))] = trim($value);
					}
				}

				if(isset($headers['upgrade']))
				{
					$this->upgradeProtocol($headers['upgrade'], $socketIdentifier);
					$accept = $this->calculateAccept($headers['sec-websocket-key']);
					
					$streamWriter = $this->server->getStreamWriter($socketIdentifier);
					$response = "HTTP/1.1 101 Switching Protocols\r\n" .
					            "Upgrade: websocket\r\n" .
								"Connection: Upgrade\r\n" .
								"Sec-WebSocket-Accept: {$accept}\r\n\r\n";
					$streamWriter->appendOutgoingData($response);
				}
				break;

			case self::PROTOCOL_WEBSOCKET:
				$headers = $this->extractHeaders($data);
				if(0 == $headers['fin'])
				{
					// put in buffer
				}


				Ype::message('executePhpCode', array("?>{$data}"));
				break;
		}
	}
}