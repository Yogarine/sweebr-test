<?php
/*  Ype - A PHP command line environment.
    Copyright © 2011-2016 Alwin Garside
    All rights reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

        1. Redistributions of source code must retain the above copyright
           notice, this list of conditions and the following disclaimer.

        2. Redistributions in binary form must reproduce the above copyright
           notice, this list of conditions and the following disclaimer in the
           documentation and/or other materials provided with the distribution.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
    IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
    THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
    PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
    CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
    EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
    PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
    OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
    WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
    OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
    ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

    The views and conclusions contained in the software and documentation are
    those of the authors and should not be interpreted as representing official
    policies, either expressed or implied, of the copyright holders. */

/**
 * @package   Ype\Broker
 * @author    Alwin Garside <alwin@garsi.de>
 * @copyright Copyright © 2011-2016 Alwin Garside
 * @license   http://www.freebsd.org/copyright/freebsd-license.html FreeBSD License
 */

/** We depend on lcfirst() */
require_once('ype_inject_compatibility_functions.php');

/**
 * @package Ype\Broker
 */
class Ype_Broker_Internal extends Ype_Broker
{
	/** @var Ype_NonBlockingStream_Process[] */
	protected $processes = array();

	/** @var Ype_ProcessCompatiblePluginInterface[] */
	protected $processPlugins = array();

	/** @var Ype_NonBlockingStream_Socket_Server */
	protected $socketServer;

	/** @var array Two dimensional array that keeps track of which sockets have which plugins. */
	protected $socketPlugins = array();

	/** @var string[] Array that keeps track of which plugins are in which sockets. */
	protected $pluginSockets = array();

	/** @var string[] List of line buffers indexed by connectionId. */
	protected $lineBuffers = array();

	public function __construct()
	{
		parent::__construct();

		$this->socketServer = $this->_startSocketServer();
	}

	public function registerPlugin(Ype_AbstractPlugin $plugin)
	{
		if($plugin instanceof Ype_ProcessCompatiblePluginInterface)
		{
			$process = $this->_startPluginProcess($plugin);
			$this->processes[$plugin->getIdentifier()]       = $process;
			$this->processPlugins[$process->getIdentifier()] = $plugin;
		}

		if(parent::registerPlugin($plugin))
		{
			// If this is a Process compatible plugin, let the process' own broker take care of it, otherwise signal
			// that the process should initialize itself.
			if(!$plugin instanceof Ype_ProcessCompatiblePluginInterface)
			{
				$this->sendMessage('initializePlugin', array(), $plugin->getIdentifier());
			}
			$this->sendMessage('pluginRegistered', array(), null, null, $plugin->getIdentifier());
		}
	}
	
	public function unregisterPlugin($pluginId)
	{
		Ype_Log::debugFunctionCall();

		if(isset($this->processes[$pluginId]))
		{
			$this->processes[$pluginId]->close();
			unset($this->processes[$pluginId]);
		}
		parent::unregisterPlugin($pluginId);
	}

	/**
	 * @param callable $callback
	 * @param array    $params
	 * @param int      $contextPluginId
	 * @throws Exception
	 */
	protected function callback($callback, $params, $contextPluginId)
	{
		$plugin = $callback[0];
		$method = $callback[1];

		if($plugin instanceof Ype_ProcessCompatiblePluginInterface)
		{
			// Remove the "on" from the start.
			$messageName  = substr($method, 2);
			$messageName  = lcfirst($messageName);
			$messageToken = $this->currentMessageToken;

			$pluginId = $plugin->getIdentifier();

			// If we know which socket the plugin lives behind, we provide the stream identifier.
			// Otherwise we just broadcast it to all sockets.
			if(isset($this->pluginSockets[$pluginId]))
			{
				$this->socketServer->appendOutgoingData(
					self::encodeMessage($messageName, $params, $messageToken, $this->senderPluginId, $pluginId),
					$this->pluginSockets[$pluginId]
				);
			}
			else
			{
				$this->socketServer->appendOutgoingData(
					self::encodeMessage($messageName, $params, $messageToken, $this->senderPluginId, $pluginId)
				);
			}
		}
		elseif($plugin instanceof Ype_PluginInterface)
		{
			$oldSenderPluginId            = $this->senderPluginId;
			$this->senderPluginId         = $this->currentContextPluginId;
			$this->currentContextPluginId = $plugin->getIdentifier();
			call_user_func_array($callback, $params);
			$this->currentContextPluginId = $this->senderPluginId;
			$this->senderPluginId         = $oldSenderPluginId;
		}
		else
		{
			throw new Exception("Only callbacks to Plugins are supported for now.");
		}
	}

	public function onSocketServerConnect($streamIdentifier)
	{
		$this->lineBuffers[$streamIdentifier] = '';
	}

	/**
	 * @param string $line
	 * @param int    $streamIdentifier
	 */
	public function onSocketServerRead($line, $streamIdentifier)
	{
		$this->lineBuffers[$streamIdentifier] .= $line;
		if(substr($this->lineBuffers[$streamIdentifier], -1) === "\n")
		{
			list($messageName, $params, $messageToken, $senderId, $targetId) = self::decodeMessage($line);
			if(!isset($this->socketPlugins[$streamIdentifier][$senderId]))
			{
				$this->socketPlugins[$streamIdentifier][$senderId] = true;
				$this->pluginSockets[$senderId]                    = $streamIdentifier;
			}
			self::sendMessage($messageName, $params, $targetId, $messageToken, $senderId);
		}
	}

	/**
	 * @param string                        $line
	 * @param Ype_NonBlockingStream_Process $process
	 */
	public function onProcessRead($line, Ype_NonBlockingStream_Process $process)
	{
		$processIdentifier = $process->getIdentifier();
		if(isset($this->processPlugins[$processIdentifier]))
		{
			$targetId = $this->processPlugins[$processIdentifier]->getIdentifier();
			// TODO send to default output handler
			self::sendMessage('processStandardOutput', array($line), $targetId);
		}
		else
		{
			Ype_Log::warning('Broker_Internal', "Missing Plugin for Process '{$processIdentifier}'");
		}
	}


	/**
	 * @param string                        $line
	 * @param Ype_NonBlockingStream_Process $process
	 */
	public function onProcessError($line, Ype_NonBlockingStream_Process $process)
	{
		$processIdentifier = $process->getIdentifier();
		if(isset($this->processPlugins[$processIdentifier]))
		{
			$targetId = $this->processPlugins[$processIdentifier]->getIdentifier();
			self::sendMessage('processStandardError', array($line), $targetId);
		}
		else
		{
			Ype_Log::warning('Broker_Internal', "Missing Plugin for Process '{$processIdentifier}'");
		}
	}

	public function closeAll()
	{
		foreach($this->processes as $process)
		{
			$process->close();
		}
		$this->socketServer->stop();
	}

	/**
	 * @return Ype_NonBlockingStream_Socket_Server
	 */
	private function _startSocketServer()
	{
		$socketFile = Ype::ensureConfigPath(getmypid() . '.socket');

		$socketServer = new Ype_NonBlockingStream_Socket_Server($this->streamHandler, $socketFile);
		$socketServer->registerConnectCallback(array($this, 'onSocketServerConnect'));
		$socketServer->registerReadCallback(array($this, 'onSocketServerRead'));
		$socketServer->run();

		return $socketServer;
	}

	/**
	 * @param Ype_ProcessCompatiblePluginInterface $plugin
	 * @return Ype_NonBlockingStream_Process
	 */
	private function _startPluginProcess(Ype_ProcessCompatiblePluginInterface $plugin)
	{
		$socketServer = $this->socketServer;

		$env       = array();
		$arguments = array(
			'--plugin',         $plugin->getName(),
			'--identifier',     $plugin->getIdentifier(),
			'--confdir',        Ype::getConfDir(),
			'--server-address', $socketServer->getAddress(),
			'--server-port',    $socketServer->getPort(),
		);

		// TODO temp debug stuff
		if($plugin instanceof Ype_Plugin_TextCanvas)
		{
			$env['XDEBUG_CONFIG'] = 'idekey=YPE';
		}

		$process = new Ype_NonBlockingStream_Process($this->streamHandler, YPE_EXECUTABLE, $arguments, null, $env);
		$process->registerReadCallback(array($this, 'onProcessRead'));
//		$process->registerErrorCallback(array($this, 'onProcessError'));
		$process->open();

		return $process;
	}

	public function quit()
	{
		$this->socketServer->stopListening();
	}
}