<?php
/*  Ype - A PHP command line environment.
    Copyright © 2011-2016 Alwin Garside
    All rights reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

        1. Redistributions of source code must retain the above copyright
           notice, this list of conditions and the following disclaimer.

        2. Redistributions in binary form must reproduce the above copyright
           notice, this list of conditions and the following disclaimer in the
           documentation and/or other materials provided with the distribution.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
    IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
    THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
    PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
    CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
    EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
    PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
    OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
    WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
    OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
    ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

    The views and conclusions contained in the software and documentation are
    those of the authors and should not be interpreted as representing official
    policies, either expressed or implied, of the copyright holders. */

/**
 * @package   Ype\NonBlockingStream
 * @author	  Alwin Garside <alwin@garsi.de>
 * @copyright Copyright © 2011-2016 Alwin Garside
 * @license   http://www.freebsd.org/copyright/freebsd-license.html FreeBSD License
 */

/**
 * @package Ype\NonBlockingStream
 */
class Ype_NonBlockingStream_Reader extends Ype_NonBlockingStream_Wrapper
{
	/** Read data chunk length. */
	const DEFAULT_BUFFER_SIZE = 8192;

	const READ_MODE_EOF = 'eof';

	const READ_MODE_EOL = 'eol';

	protected $readMode = self::READ_MODE_EOL;
	
	protected $readBufferSize = self::DEFAULT_BUFFER_SIZE;

	/** @var callable[] */
	protected $callbacks = array();

	/** @var boolean */
	protected $readClosed = false;

	public function __construct($stream, Ype_NonBlockingStream_Handler $handler)
	{
		parent::__construct($stream, $handler);

		stream_set_read_buffer($this->stream, $this->readBufferSize);
	}

	/**
	 * Make sure this stream is closed on destruction.
	 */
	public function __destruct()
	{
		$this->close();
	}

	/**
	 * @return string
	 */
	public function getReadMode()
	{
		return $this->readMode;
	}

	/**
	 * @param string $readMode
	 */
	public function setReadMode($readMode)
	{
		$this->readMode = $readMode;
	}

	/**
	 * @param boolean  $withTimeout
	 * @param int      $timeoutDuration
	 */
	public function registerForRead($withTimeout = false, $timeoutDuration = 5)
	{
		$this->handler->registerReadStream($this->stream, array($this, 'read'), $withTimeout ? array($this, 'onTimeOut') : null,
		                                   $timeoutDuration);
	}

	/**
	 * Unregister for read.
	 */
	public function unregisterForRead()
	{
		Ype_Log::debugFunctionCall();
		$this->handler->unregisterReadStream($this->stream);
	}

	/**
	 * @param callable $callback
	 */
	public function registerReadCallback($callback)
	{
		// TODO some sanity checks.
		$this->callbacks[] = $callback;
	}

	/**
	 * @param string $line
	 */
	protected function callback($line)
	{
		foreach($this->callbacks as $callback)
		{
			// XXX debug stuff
			$object = $callback[0];
			$method = $callback[1];
			if(is_object($object))
			{
				$object = get_class($object);
			}

			Ype_Log::debug(__CLASS__, "Calling callback {$object}::{$method}().");

			call_user_func($callback, $line, $this->streamIdentifier);
		}
	}

	public function read($stream)
	{
		switch($this->readMode)
		{
			case self::READ_MODE_EOL:
				$data = fgets($stream, $this->readBufferSize);
				break;

			case self::READ_MODE_EOF:
				$data = fread($stream, $this->readBufferSize);
				break;

			default:
				throw new Exception("Invalid read mode: '{$this->readMode}'");
		}

		Ype_Log::debug(__CLASS__, "Read data: '{$data}' (" . strlen($data) . ")");

		if(false === $data || strlen($data) === 0)
		{
			$this->callbackOnDisconnect($this->streamIdentifier);
			$this->close();
			Ype_Log::error(__CLASS__, "Failed to read from socket '{$this->stream}'");
		}
		else
		{
			$this->callback($data);
		}
	}

	/**
	 * Close.
	 */
	public function close()
	{
		Ype_Log::debugFunctionCall($this->stream);

		$this->unregisterForRead();

		if(is_resource($this->stream))
		{
			fclose($this->stream);
		}
		$this->stream = null;
	}

}
