<?php
/*  Ype - A PHP command line environment.
    Copyright © 2011-2016 Alwin Garside
    All rights reserved.

    Redistribution and use in source and binary forms, with or without
    modification, are permitted provided that the following conditions are met:

        1. Redistributions of source code must retain the above copyright
           notice, this list of conditions and the following disclaimer.

        2. Redistributions in binary form must reproduce the above copyright
           notice, this list of conditions and the following disclaimer in the
           documentation and/or other materials provided with the distribution.

    THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS
    IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
    THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
    PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
    CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
    EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
    PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
    OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
    WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
    OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
    ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

    The views and conclusions contained in the software and documentation are
    those of the authors and should not be interpreted as representing official
    policies, either expressed or implied, of the copyright holders. */

/**
 * @package   Ype\NonBlockingStream
 * @author    Alwin Garside <alwin@garsi.de>
 * @copyright Copyright © 2011-2016 Alwin Garside
 * @license   http://www.freebsd.org/copyright/freebsd-license.html FreeBSD License
 */

/**
 * @package Ype\NonBlockingStream
 */
class Ype_NonBlockingStream_Process
{
	/** Standard input */
	const PIPE_STDIN = 0;
	/** Standard output */
	const PIPE_STDOUT = 1;
	/** Standard error */
	const PIPE_STDERR = 2;

	/** Status element containing the command string that was passed to proc_open(). */
	const STATUS_COMMAND = 'command';
	/** Status element containing process id. */
	const STATUS_PID = 'pid';
	/** Status element for whether the process is still running. */
	const STATUS_RUNNING = 'running';
	/** Status element for whether the child process has been  terminated by an uncaught signal. */
	const STATUS_SIGNALED = 'signaled';
	/** Status element for whether the child process has been stopped by a signal. */
	const STATUS_STOPPED = 'stopped';
	/** Status element containing the exit code returned by the process. */
	const STATUS_EXITCODE = 'exitcode';
	/** Status element containing the number of the signal that caused the child process to terminate its execution. */
	const STATUS_TERMSIG = 'termsig';
	/** Status element containing the number of the signal that caused the child process to stop its execution. */
	const STATUS_STOPSIG = 'stopsig';

	/** In progress error code. */
	const EAGAIN = 36; // in progress error

	/** @var int */
	protected $identifier;

	protected $streamIdentifier = null;

	protected $phpFile;

	protected $arguments;

	protected $cwd;

	protected $env;

	protected $otherOptions;

	/** @var resource The resource handle for this process */
	protected $process = null;

	/** @var Ype_NonBlockingStream_Writer */
	protected $stdinWriter;

	/** @var Ype_NonBlockingStream_Reader */
	protected $stdoutReader;

	/** @var Ype_NonBlockingStream_Reader */
	protected $stderrReader;

	/** @var resource The stdin resource for this process */
	protected $stdin = null;

	/** @var resource The stdout resource for this process */
	protected $stdout = null;

	/** @var resource The stderr resource for this process */
	protected $stderr = null;

	/** @var Ype_NonBlockingStream_Handler */
	protected $handler;

	/** @var resource[] */
	protected $pipes = array();

	/** @var callable[] */
	protected $readCallbacks = array();

	protected $errorCallbacks = array();

	static protected $identifierIndex = 0;

	/**
	 * @param Ype_NonBlockingStream_Handler $handler
	 * @param string                        $phpFile
	 * @param array                         $arguments
	 * @param string                        $cwd
	 * @param array                         $env
	 * @param array                         $otherOptions
	 * @throws Exception
	 */
	public function __construct(Ype_NonBlockingStream_Handler $handler, $phpFile, array $arguments = array(),
	                            $cwd = null, array $env = null, array $otherOptions = array())
	{
		$this->identifier   = self::$identifierIndex++;
		
		$this->handler      = $handler;
		$this->arguments    = $arguments;
		$this->cwd          = $cwd;
		$this->env          = $env;
		$this->otherOptions = $otherOptions;

		$phpFile = realpath($phpFile);
		if(false == $phpFile)
		{
			throw new Exception("Couldn't find PHP file '{$phpFile}'");
		}
		$this->phpFile = $phpFile;
	}

	public function open()
	{
		Ype_Log::debugFunctionCall();

		if(null !== $this->process)
		{
			Ype_Log::warning(__CLASS__, "Process is already running: '{$this->streamIdentifier}'");
		}

		$arguments = array_filter($this->arguments, 'escapeshellarg');
		$command   = "php \"{$this->phpFile}\" " . implode(' ', $arguments);

		$descriptorSpec = array(
			self::PIPE_STDIN  => array('pipe', 'r'), // stdin is a pipe that the child will read from
			self::PIPE_STDOUT => array('pipe', 'w'), // stdout is a pipe that the child will write to
			self::PIPE_STDERR => array('pipe', 'a'), // stderr is a pipe that the child will write to
		);

		Ype_Log::debug(__CLASS__, "command: '{$command}'");
		$this->process = proc_open($command, $descriptorSpec, $this->pipes, $this->cwd, $this->env, $this->otherOptions);
		Ype_Log::debug(__CLASS__, "process: {$this->process}");

		$this->streamIdentifier = (string) $this->process;

		if(is_resource($this->process))
		{
			$this->stdin  = $this->pipes[self::PIPE_STDIN];
			$this->stdout = $this->pipes[self::PIPE_STDOUT];
			$this->stderr = $this->pipes[self::PIPE_STDERR];
		}
		else
		{
			$this->process = null;
			throw new Exception('Unable to open session process.');
		}

		$this->streamIdentifier = (string) $this->process;

		$this->stdinWriter  = new Ype_NonBlockingStream_Writer($this->stdin, $this->handler);
		$this->stdoutReader = new Ype_NonBlockingStream_Reader($this->stdout, $this->handler);
		$this->stderrReader = new Ype_NonBlockingStream_Reader($this->stderr, $this->handler);

		$this->stdoutReader->registerReadCallback(array($this, 'onRead'));
		$this->stderrReader->registerReadCallback(array($this, 'onError'));

		$this->stdoutReader->registerForRead();
		$this->stderrReader->registerForRead();
	}

	/**
	 * @param string $line
	 * @param string $streamIdentifier
	 */
	public function onRead($line, $streamIdentifier)
	{
		foreach($this->readCallbacks as $readCallback)
		{
			call_user_func($readCallback, $line, $this);
		}
	}

	/**
	 * @param string $line
	 */
	public function onError($line)
	{
		foreach($this->errorCallbacks as $errorCallback)
		{
			call_user_func($errorCallback, $line, $this);
		}
	}

	/**
	 * @param callable $callback
	 */
	public function registerReadCallback($callback)
	{
		$this->readCallbacks[] = $callback;
	}

	/**
	 * @param callable $callback
	 */
	public function registerErrorCallback($callback)
	{
		$this->errorCallbacks[] = $callback;
	}

	/**
	 * @return boolean
	 */
	public function isRunning()
	{
		return $this->process !== null;
	}

	public function close()
	{
		Ype_Log::debugFunctionCall();

		$this->stdoutReader->close();
		$this->stderrReader->close();
		$this->stdinWriter->close();
		
		unset($this->process);
		$this->process = null;
	}

	/**
	 * @return int
	 */
	public function getIdentifier()
	{
		return $this->identifier;
	}
}