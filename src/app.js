export class App {
    message = 'Welcome to Aurelia!';

    chat = '';

    constructor()
    {
        var socket = new WebSocket("ws://localhost:31337");

        socket.onmessage = function(event)
        {
            console.log('onmessage', event);
            this.message = this.message + event.data + "\n";
        }.bind(this);

        socket.onopen = function(event)
        {
            console.log('onopen', event);
            socket.send("hello world");
        };
    }
}
